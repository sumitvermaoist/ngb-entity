package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.PrepaidMeterRebateInterface;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity(name = "PrepaidMeterRebate")
@Table(name = "prepaid_meter_rebate")
public class PrepaidMeterRebate implements PrepaidMeterRebateInterface {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "start_consumption")
    private long startConsumption;

    @Column(name = "end_consumption")
    private long endConsumption;

    @Column(name = "rate")
    private String rate;

    @Column(name = "multiplier")
    private BigDecimal multiplier;

    @Column(name = "effective_start_date")
    @Temporal(TemporalType.DATE)
    private Date effectiveStartDate;

    @Column(name = "effective_end_date")
    @Temporal(TemporalType.DATE)
    private Date effectiveEndDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getStartConsumption() {
        return startConsumption;
    }

    public void setStartConsumption(long startConsumption) {
        this.startConsumption = startConsumption;
    }

    public long getEndConsumption() {
        return endConsumption;
    }

    public void setEndConsumption(long endConsumption) {
        this.endConsumption = endConsumption;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public BigDecimal getMultiplier() {
        if(this.multiplier != null){
            double temp = this.multiplier.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return multiplier;
    }

    public void setMultiplier(BigDecimal multiplier) {
        this.multiplier = multiplier;
    }

    public Date getEffectiveStartDate() {
        return effectiveStartDate;
    }

    public void setEffectiveStartDate(Date effectiveStartDate) {
        this.effectiveStartDate = effectiveStartDate;
    }

    public Date getEffectiveEndDate() {
        return effectiveEndDate;
    }

    public void setEffectiveEndDate(Date effectiveEndDate) {
        this.effectiveEndDate = effectiveEndDate;
    }

    @Override
    public String toString() {
        return "PrepaidMeterRebate{" +
                "id=" + id +
                ", startConsumption=" + startConsumption +
                ", endConsumption=" + endConsumption +
                ", rate='" + rate + '\'' +
                ", multiplier=" + multiplier +
                ", effectiveStartDate=" + effectiveStartDate +
                ", effectiveEndDate=" + effectiveEndDate +
                '}';
    }
}
