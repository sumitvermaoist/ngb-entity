package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.EmployeeConnectionMappingInterface;
import com.mppkvvcl.ngbinterface.interfaces.EmployeeMasterInterface;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by PREETESH on 6/8/2017.
 */
@Entity(name = "EmployeeConnectionMapping")
@Table(name = "employee_connection_mapping")
public class EmployeeConnectionMapping implements EmployeeConnectionMappingInterface {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "employee_no")
    private String employeeNo;

    @Column(name = "consumer_no")
    private String consumerNo;

    @Column(name = "status")
    private String status;

    @Column(name = "start_date")
    @Temporal(TemporalType.DATE)
    private Date startDate;

    @Column(name = "start_bill_month")
    private String startBillMonth;

    @Column(name = "end_date")
    @Temporal(TemporalType.DATE)
    private Date endDate;

    @Column(name = "end_bill_month")
    private String endBillMonth;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    @OneToOne
    @JoinColumn(name = "employee_no",referencedColumnName = "employee_no",updatable = false,insertable = false)
    private EmployeeMaster employeeMaster;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmployeeNo() {
        return employeeNo;
    }

    public void setEmployeeNo(String employeeNo) {
        this.employeeNo = employeeNo;
    }

    public String getConsumerNo() {
        return consumerNo;
    }

    public void setConsumerNo(String consumerNo) {
        this.consumerNo = consumerNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getStartBillMonth() {
        return startBillMonth;
    }

    public void setStartBillMonth(String startBillMonth) {
        this.startBillMonth = startBillMonth;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getEndBillMonth() {
        return endBillMonth;
    }

    public void setEndBillMonth(String endBillMonth) {
        this.endBillMonth = endBillMonth;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public EmployeeMasterInterface getEmployeeMaster() {
        return employeeMaster;
    }

    public void setEmployeeMaster(EmployeeMasterInterface employeeMasterInterface) {
        if(employeeMasterInterface != null && employeeMasterInterface instanceof EmployeeMaster){
            this.employeeMaster = (EmployeeMaster) employeeMasterInterface;
        }
    }

    @Override
    public String toString() {
        return "EmployeeConnectionMapping{" +
                "id=" + id +
                ", employeeNo='" + employeeNo + '\'' +
                ", consumerNo='" + consumerNo + '\'' +
                ", status='" + status + '\'' +
                ", startDate=" + startDate +
                ", startBillMonth='" + startBillMonth + '\'' +
                ", endDate=" + endDate +
                ", endBillMonth='" + endBillMonth + '\'' +
                ", createdOn=" + createdOn +
                ", createdBy='" + createdBy + '\'' +
                ", updatedOn=" + updatedOn +
                ", updatedBy='" + updatedBy + '\'' +
                '}';
    }
}
