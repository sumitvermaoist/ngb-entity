package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.LoadFactorIncentiveConfigurationInterface;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by PREETESH on 12/22/2017.
 */
@Entity(name = "LoadFactorIncentiveConfiguration")
@Table(name = "load_factor_incentive_configuration")
public class LoadFactorIncentiveConfiguration implements LoadFactorIncentiveConfigurationInterface {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "start_range")
    private BigDecimal startRange;

    @Column(name = "end_range")
    private BigDecimal endRange;

    @Column(name = "rate")
    private String rate;

    @Column(name = "multiplier")
    private BigDecimal multiplier;

    @Column(name = "start_date")
    @Temporal(TemporalType.DATE)
    private Date startDate;

    @Column(name = "end_date")
    @Temporal(TemporalType.DATE)
    private Date endDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BigDecimal getStartRange() {
        if(this.startRange != null){
            double temp = this.startRange.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return startRange;
    }

    public void setStartRange(BigDecimal startRange) {
        this.startRange = startRange;
    }

    public BigDecimal getEndRange() {
        if(this.endRange != null){
            double temp = this.endRange.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return endRange;
    }

    public void setEndRange(BigDecimal endRange) {
        this.endRange = endRange;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public BigDecimal getMultiplier() {
        if(this.multiplier != null){
            double temp = this.multiplier.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return multiplier;
    }

    public void setMultiplier(BigDecimal multiplier) {
        this.multiplier = multiplier;
    }

    @Override
    public String toString() {
        return "LoadFactorIncentiveConfiguration{" +
                "id=" + id +
                ", startRange=" + startRange +
                ", endRange=" + endRange +
                ", rate='" + rate + '\'' +
                ", multiplier=" + multiplier +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }
}
