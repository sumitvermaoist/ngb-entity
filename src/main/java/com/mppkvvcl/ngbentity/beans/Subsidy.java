package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.SubsidyInterface;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Table(name = "subsidy")
@Entity(name = "Subsidy")
public class Subsidy implements SubsidyInterface {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "category")
    private String category;

    @Column(name = "is_bpl")
    private boolean isBpl;

    @Column(name = "subsidy")
    private BigDecimal subsidy;

    @Column(name = "slab_start")
    private long slabStart;

    @Column(name = "slab_end")
    private long slabEnd;

    @Column(name = "subcategory_code")
    private long subcategoryCode;

    @Column(name = "effective_start_date")
    @Temporal(TemporalType.DATE)
    private Date effectiveStartDate;

    @Column(name = "effective_end_date")
    @Temporal(TemporalType.DATE)
    private Date effectiveEndDate;

    @Column(name = "head")
    private String head;

    @Column(name = "multiplier")
    private BigDecimal multiplier;

    @Column(name = "subcategory_violation")
    private boolean subcategoryViolation;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public boolean isBpl() {
        return isBpl;
    }

    public void setBpl(boolean bpl) {
        isBpl = bpl;
    }

    public BigDecimal getSubsidy() {
        if(this.subsidy != null){
            double temp = this.subsidy.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return subsidy;
    }

    public void setSubsidy(BigDecimal subsidy) {
        this.subsidy = subsidy;
    }

    public long getSlabStart() {
        return slabStart;
    }

    public void setSlabStart(int slabStart) {
        this.slabStart = slabStart;
    }

    public long getSlabEnd() {
        return slabEnd;
    }

    public void setSlabEnd(int slabEnd) {
        this.slabEnd = slabEnd;
    }

    public long getSubcategoryCode() {
        return subcategoryCode;
    }

    public void setSubcategoryCode(long subcategoryCode) {
        this.subcategoryCode = subcategoryCode;
    }

    public Date getEffectiveStartDate() {
        return effectiveStartDate;
    }

    public void setEffectiveStartDate(Date effectiveStartDate) {
        this.effectiveStartDate = effectiveStartDate;
    }

    public Date getEffectiveEndDate() {
        return effectiveEndDate;
    }

    public void setEffectiveEndDate(Date effectiveEndDate) {
        this.effectiveEndDate = effectiveEndDate;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public BigDecimal getMultiplier() {
        if(this.multiplier != null){
            double temp = this.multiplier.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return multiplier;
    }

    public void setMultiplier(BigDecimal multiplier) {
        this.multiplier = multiplier;
    }

    public boolean isSubcategoryViolation() {
        return subcategoryViolation;
    }

    public void setSubcategoryViolation(boolean subcategoryViolation) {
        this.subcategoryViolation = subcategoryViolation;
    }

    @Override
    public String toString() {
        return "Subsidy{" +
                "id=" + id +
                ", category='" + category + '\'' +
                ", isBpl=" + isBpl +
                ", subsidy=" + subsidy +
                ", slabStart=" + slabStart +
                ", slabEnd=" + slabEnd +
                ", subcategoryCode=" + subcategoryCode +
                ", effectiveStartDate=" + effectiveStartDate +
                ", effectiveEndDate=" + effectiveEndDate +
                ", head='" + head + '\'' +
                ", multiplier=" + multiplier +
                ", subcategoryViolation=" + subcategoryViolation +
                '}';
    }
}
