package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.PowerFactorInterface;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Table(name = "power_factor")
@Entity(name = "PowerFactor")
public class PowerFactor implements PowerFactorInterface {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "charge_code")
    private String chargeCode;

    @Column(name = "start_range")
    private BigDecimal startRange;

    @Column(name = "end_range")
    private BigDecimal endRange;

    @Column(name = "rate")
    private BigDecimal rate;

    @Column(name = "start_date")
    @Temporal(TemporalType.DATE)
    private Date startDate;

    @Column(name = "end_date")
    @Temporal(TemporalType.DATE)
    private Date endDate;

    @Column(name = "multiplier")
    private BigDecimal multiplier;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getChargeCode() {
        return chargeCode;
    }

    public void setChargeCode(String chargeCode) {
        this.chargeCode = chargeCode;
    }

    public BigDecimal getStartRange() {
        if(this.startRange != null){
            double temp = this.startRange.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return startRange;
    }

    public void setStartRange(BigDecimal startRange) {
        this.startRange = startRange;
    }

    public BigDecimal getEndRange() {
        if(this.endRange != null){
            double temp = this.endRange.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return endRange;
    }

    public void setEndRange(BigDecimal endRange) {
        this.endRange = endRange;
    }

    public BigDecimal getRate() {
        if(this.rate != null){
            double temp = this.rate.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public BigDecimal getMultiplier() {
        if(this.multiplier != null){
            double temp = this.multiplier.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return multiplier;
    }

    public void setMultiplier(BigDecimal multiplier) {
        this.multiplier = multiplier;
    }

    @Override
    public String toString() {
        return "PowerFactor{" +
                "id=" + id +
                ", chargeCode='" + chargeCode + '\'' +
                ", startRange=" + startRange +
                ", endRange=" + endRange +
                ", rate=" + rate +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", multiplier=" + multiplier +
                '}';
    }
}
