package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.ConnectionTypeInterface;

import javax.persistence.*;

/**
 * Created by ANSHIKA on 18-07-2017.
 */
@Entity(name = "ConnectionType")
@Table(name = "connection_type")
public class ConnectionType implements ConnectionTypeInterface {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private long id;

    @Column(name = "type")
    private String type;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "ConnectionType{" +
                "id=" + id +
                ", type='" + type + '\'' +
                '}';
    }
}
