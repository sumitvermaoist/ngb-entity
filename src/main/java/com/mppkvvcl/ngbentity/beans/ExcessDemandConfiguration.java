package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.ExcessDemandConfigurationInterface;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Table(name = "excess_demand_configuration")
@Entity(name = "ExcessDemandConfiguration")
public class ExcessDemandConfiguration implements ExcessDemandConfigurationInterface{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "value")
    private String value;

    @Column(name = "multiplier")
    private BigDecimal multiplier;

    @Column(name = "penal_charge")
    private String penalCharge;

    @Column(name = "penal_charge_multiplier")
    private BigDecimal penalChargeMultiplier;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public BigDecimal getMultiplier() {
        if(this.multiplier != null){
            double temp = this.multiplier.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return multiplier;
    }

    public void setMultiplier(BigDecimal multiplier) {
        this.multiplier = multiplier;
    }

    public String getPenalCharge() {
        return penalCharge;
    }

    public void setPenalCharge(String penalCharge) {
        this.penalCharge = penalCharge;
    }

    public BigDecimal getPenalChargeMultiplier() {
        if(this.penalChargeMultiplier != null){
            double temp = this.penalChargeMultiplier.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return penalChargeMultiplier;
    }

    public void setPenalChargeMultiplier(BigDecimal penalChargeMultiplier) {
        this.penalChargeMultiplier = penalChargeMultiplier;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    @Override
    public String toString() {
        return "ExcessDemandConfiguration{" +
                "id=" + id +
                ", value='" + value + '\'' +
                ", multiplier=" + multiplier +
                ", penalCharge='" + penalCharge + '\'' +
                ", penalChargeMultiplier=" + penalChargeMultiplier +
                ", createdBy='" + createdBy + '\'' +
                ", createdOn=" + createdOn +
                '}';
    }
}
