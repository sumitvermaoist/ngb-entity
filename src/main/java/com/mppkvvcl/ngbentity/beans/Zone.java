package com.mppkvvcl.ngbentity.beans;


import com.mppkvvcl.ngbinterface.interfaces.DivisionInterface;
import com.mppkvvcl.ngbinterface.interfaces.ZoneInterface;

import javax.persistence.*;

/**
 * Created by ANSHIKA on 17-07-2017.
 */
@Entity(name = "Zone")
@Table(name = "zone")
public class Zone implements ZoneInterface {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private long id;

    @Column(name = "code")
    private String code;

    @Column(name = "name")
    private String name;

    @Column(name = "short_code")
    private String shortCode;

    @Column(name = "address")
    private String address;

    @Column(name = "phone_no")
    private String phoneNo;

    /*@Column(name = "division_id")
    private long divisionId;*/

    @OneToOne
    @JoinColumn(name = "division_id")
    private Division division;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortCode() {
        return shortCode;
    }

    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public DivisionInterface getDivision() {
        return division;
    }

    public void setDivision(DivisionInterface division) {
        if(division != null && division instanceof Division){
            this.division = (Division)division;
        }
    }
}
