package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.SecurityDepositPostedInterface;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by PREETESH on 11/17/2017.
 */
@Entity(name = "security_deposit_posted")
@Table(name = "SecurityDepositPosted")
public class SecurityDepositPosted implements SecurityDepositPostedInterface {

    @Id
    @Column(name= "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "security_deposit_id" )
    private long securityDepositId;

    @Column(name = "posted_amount")
    private BigDecimal postedAmount;

    @Column(name = "bill_month" )
    private String billMonth;

    @Column(name = "source" )
    private String source;

    @Column(name = "effective_start_date")
    @Temporal(TemporalType.DATE)
    private Date effectiveStartDate;

    @Column(name = "applied_on_bill")
    private boolean appliedOnBill;

    @Column(name = "created_by")
    private String createdBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_on")
    private Date createdOn;

    @Column(name = "updated_by")
    private String updatedBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_on")
    private Date updatedOn;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getSecurityDepositId() {
        return securityDepositId;
    }

    public void setSecurityDepositId(long securityDepositId) {
        this.securityDepositId = securityDepositId;
    }

    public BigDecimal getPostedAmount() {
        return postedAmount;
    }

    public void setPostedAmount(BigDecimal postedAmount) {
        this.postedAmount = postedAmount;
    }

    public String getBillMonth() {
        return billMonth;
    }

    public void setBillMonth(String billMonth) {
        this.billMonth = billMonth;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Date getEffectiveStartDate() {
        return effectiveStartDate;
    }

    public void setEffectiveStartDate(Date effectiveStartDate) {
        this.effectiveStartDate = effectiveStartDate;
    }

    public boolean isAppliedOnBill() {
        return appliedOnBill;
    }

    public void setAppliedOnBill(boolean appliedOnBill) {
        this.appliedOnBill = appliedOnBill;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public Date getCreatedOn() {
        return createdOn;
    }

    @Override
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public Date getUpdatedOn() {
        return updatedOn;
    }

    @Override
    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Override
    public String toString() {
        return "SecurityDepositPosted{" +
                "id=" + id +
                ", securityDepositId=" + securityDepositId +
                ", postedAmount=" + postedAmount +
                ", billMonth='" + billMonth + '\'' +
                ", source='" + source + '\'' +
                ", effectiveStartDate=" + effectiveStartDate +
                ", appliedOnBill=" + appliedOnBill +
                ", createdBy='" + createdBy + '\'' +
                ", createdOn=" + createdOn +
                ", updatedBy='" + updatedBy + '\'' +
                ", updatedOn=" + updatedOn +
                '}';
    }
}
