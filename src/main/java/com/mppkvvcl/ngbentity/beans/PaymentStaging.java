package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.PaymentStagingInterface;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by PREETESH on 1/2/2018.
 */
@Entity(name = "PaymentStaging")
@Table(name = "payment_staging")
public class PaymentStaging implements PaymentStagingInterface{

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private long id;

    @Column(name = "bill_id")
    private long billId;

    @Column(name = "source")
    private String source;

    @Column(name = "transaction_id")
    private String transactionId;

    @Column(name = "mode_of_payment")
    private String modeOfPayment;

    @Column(name = "consumer_name")
    private String consumerName;

    @Column(name = "service_no")
    private String serviceNo;

    @Column(name = "ivrs")
    private String ivrs;

    @Column(name = "group_no")
    private String groupNo;

    @Column(name = "reading_diary_no")
    private String readingDiaryNo;

    @Column(name = "location_code")
    private String locationCode;

    @Column(name = "amount")
    private BigDecimal amount;

    @Temporal(TemporalType.DATE)
    @Column(name = "date_of_transaction")
    private Date dateOfTransaction;

    @Temporal(TemporalType.DATE)
    @Column(name = "date_of_verification")
    private Date dateOfVerification;

    @Column(name = "erp_cra")
    private String erpCra;

    @Column(name = "payment_id")
    private long paymentId;

    @Column(name = "status")
    private String status;

    @Column(name = "description")
    private String description;

    @Column(name = "created_by")
    private String createdBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_on")
    private Date createdOn;

    @Column(name = "updated_by")
    private String updatedBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_on")
    private Date updatedOn;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public long getBillId() {
        return billId;
    }

    @Override
    public void setBillId(long billId) {
        this.billId = billId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getModeOfPayment() {
        return modeOfPayment;
    }

    public void setModeOfPayment(String modeOfPayment) {
        this.modeOfPayment = modeOfPayment;
    }

    public String getConsumerName() {
        return consumerName;
    }

    public void setConsumerName(String consumerName) {
        this.consumerName = consumerName;
    }

    public String getServiceNo() {
        return serviceNo;
    }

    public void setServiceNo(String serviceNo) {
        this.serviceNo = serviceNo;
    }

    public String getIvrs() {
        return ivrs;
    }

    public void setIvrs(String ivrs) {
        this.ivrs = ivrs;
    }

    public String getGroupNo() {
        return groupNo;
    }

    public void setGroupNo(String groupNo) {
        this.groupNo = groupNo;
    }

    public String getReadingDiaryNo() {
        return readingDiaryNo;
    }

    public void setReadingDiaryNo(String readingDiaryNo) {
        this.readingDiaryNo = readingDiaryNo;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getDateOfTransaction() {
        return dateOfTransaction;
    }

    public void setDateOfTransaction(Date dateOfTransaction) {
        this.dateOfTransaction = dateOfTransaction;
    }

    public Date getDateOfVerification() {
        return dateOfVerification;
    }

    public void setDateOfVerification(Date dateOfVerification) {
        this.dateOfVerification = dateOfVerification;
    }

    public String getErpCra() {
        return erpCra;
    }

    public void setErpCra(String erpCra) {
        this.erpCra = erpCra;
    }

    public long getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(long paymentId) {
        this.paymentId = paymentId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Override
    public String toString() {
        return "PaymentStaging{" +
                "id=" + id +
                ", billId=" + billId +
                ", source='" + source + '\'' +
                ", transactionId='" + transactionId + '\'' +
                ", modeOfPayment='" + modeOfPayment + '\'' +
                ", consumerName='" + consumerName + '\'' +
                ", serviceNo='" + serviceNo + '\'' +
                ", ivrs='" + ivrs + '\'' +
                ", groupNo='" + groupNo + '\'' +
                ", readingDiaryNo='" + readingDiaryNo + '\'' +
                ", locationCode='" + locationCode + '\'' +
                ", amount=" + amount +
                ", dateOfTransaction=" + dateOfTransaction +
                ", dateOfVerification=" + dateOfVerification +
                ", erpCra='" + erpCra + '\'' +
                ", paymentId=" + paymentId +
                ", status='" + status + '\'' +
                ", description='" + description + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", createdOn=" + createdOn +
                ", updatedBy='" + updatedBy + '\'' +
                ", updatedOn=" + updatedOn +
                '}';
    }
}
