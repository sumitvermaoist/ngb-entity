package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.SecurityDepositInterestRateInterface;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity(name = "SecurityDepositInterestRate")
@Table(name = "security_deposit_interest_rate")
public class SecurityDepositInterestRate implements SecurityDepositInterestRateInterface {
    @Id
    @Column(name= "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "rate")
    private String rate;

    @Column(name = "multiplier")
    private BigDecimal multiplier;

    @Column(name = "threshold")
    private BigDecimal threshold;

    @Column(name = " tds_rate_one")
    private String tdsRateOne ;

    @Column(name = "tds_rate_one_multiplier")
    private BigDecimal tdsRateOneMultiplier ;

    @Column(name = "tds_rate_two")
    private String tdsRateTwo;

    @Column(name = " tds_rate_two_multiplier")
    private BigDecimal tdsRateTwoMultiplier;

    @Column(name = "effective_start_date")
    @Temporal(TemporalType.DATE)
    private Date effectiveStartDate;

    @Column(name = "effective_end_date")
    @Temporal(TemporalType.DATE)
    private Date effectiveEndDate;

    @Column(name = "order_reference")
    private String orderReference;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public BigDecimal getMultiplier() {
        if(this.multiplier != null){
            double temp = this.multiplier.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return multiplier;
    }

    public void setMultiplier(BigDecimal multiplier) {
        this.multiplier = multiplier;
    }

    public BigDecimal getThreshold() {
        if(this.threshold != null){
            double temp = this.threshold.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return threshold;
    }

    public void setThreshold(BigDecimal threshold) {
        this.threshold = threshold;
    }

    public String getTdsRateOne() {
        return tdsRateOne;
    }

    public void setTdsRateOne(String tdsRateOne) {
        this.tdsRateOne = tdsRateOne;
    }

    public BigDecimal getTdsRateOneMultiplier() {
        if(this.tdsRateOneMultiplier != null){
            double temp = this.tdsRateOneMultiplier.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return tdsRateOneMultiplier;
    }

    public void setTdsRateOneMultiplier(BigDecimal tdsRateOneMultiplier) {
        this.tdsRateOneMultiplier = tdsRateOneMultiplier;
    }

    public String getTdsRateTwo() {
        return tdsRateTwo;
    }

    public void setTdsRateTwo(String tdsRateTwo) {
        this.tdsRateTwo = tdsRateTwo;
    }

    public BigDecimal getTdsRateTwoMultiplier() {
        if(this.tdsRateTwoMultiplier != null){
            double temp = this.tdsRateTwoMultiplier.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return tdsRateTwoMultiplier;
    }

    public void setTdsRateTwoMultiplier(BigDecimal tdsRateTwoMultiplier) {
        this.tdsRateTwoMultiplier = tdsRateTwoMultiplier;
    }

    public Date getEffectiveStartDate() {
        return effectiveStartDate;
    }

    public void setEffectiveStartDate(Date effectiveStartDate) {
        this.effectiveStartDate = effectiveStartDate;
    }

    public Date getEffectiveEndDate() {
        return effectiveEndDate;
    }

    public void setEffectiveEndDate(Date effectiveEndDate) {
        this.effectiveEndDate = effectiveEndDate;
    }

    public String getOrderReference() {
        return orderReference;
    }

    public void setOrderReference(String orderReference) {
        this.orderReference = orderReference;
    }

    @Override
    public String toString() {
        return "SecurityDepositInterestRate{" +
                "id=" + id +
                ", rate='" + rate + '\'' +
                ", multiplier=" + multiplier +
                ", threshold=" + threshold +
                ", tdsRateOne='" + tdsRateOne + '\'' +
                ", tdsRateOneMultiplier=" + tdsRateOneMultiplier +
                ", tdsRateTwo='" + tdsRateTwo + '\'' +
                ", tdsRateTwoMultiplier=" + tdsRateTwoMultiplier +
                ", effectiveStartDate=" + effectiveStartDate +
                ", effectiveEndDate=" + effectiveEndDate +
                ", orderReference='" + orderReference + '\'' +
                '}';
    }
}
