package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.ConsumerGovernmentMappingInterface;

import javax.persistence.*;

/**
 * Created by PREETESH on 6/8/2017.
 */
@Entity(name = "ConsumerGovernmentMapping")
@Table(name = "consumer_government_mapping")
public class ConsumerGovernmentMapping implements ConsumerGovernmentMappingInterface {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "consumer_no")
    private String consumerNo;

    @Column(name = "government_type")
    private String governmentType;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getConsumerNo() {
        return consumerNo;
    }

    public void setConsumerNo(String consumerNo) {
        this.consumerNo = consumerNo;
    }

    public String getGovernmentType() {
        return governmentType;
    }

    public void setGovernmentType(String governmentType) {
        this.governmentType = governmentType;
    }

    @Override
    public String toString() {
        return "ConsumerGovernmentMapping{" +
                "id=" + id +
                ", consumerNo='" + consumerNo + '\'' +
                ", governmentType='" + governmentType + '\'' +
                '}';
    }
}

