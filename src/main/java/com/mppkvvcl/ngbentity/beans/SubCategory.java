package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by PREETESH on 5/19/2017.
 *
 * This bean contains attributes related to subcategory of tariff , every Tariff category contains 1 to many subcategory
 * depending upon the properties of connection like Permanent,Temporary, Rural, Urban etc.
 *
 * UPDATED BY : SUMIT VERMA on 03/06/2017
 * UPDATION : Added applicant_type column mapping for considering SEASONAL consumer with bean member variable applicantType.
 * This column again can be taken as new dimension for filtering tariffCodes beside meteringStatus and connectionType
 */
@Entity(name = "subcategory")
@Table(name= "sub_category")
public class SubCategory implements SubCategoryInterface {

    @Id
    @Column(name= "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name="tariff_id")
    private long tariffId;

    @Column(name="code")
    private long code;

    @Column(name="slab_id")
    private String slabId;

    @Column(name = "description")
    private String description;

    @Column(name="start_contract_demand_kw")
    private BigDecimal startContractDemandKW;

    @Column(name="end_contract_demand_kw")
    private BigDecimal endContractDemandKW;

    @Column(name = "start_connected_load_kw")
    private BigDecimal startConnectedLoadKW;

    @Column(name = "end_connected_load_kw")
    private BigDecimal endConnectedLoadKW;

    @Column(name = "premise_type")
    private String premiseType;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "created_on")
    @Temporal(TemporalType.DATE)
    private Date createdOn;

    @Column(name = "updated_on")
    @Temporal(TemporalType.DATE)
    private Date updatedOn;

    @Column(name = "applicant_type")
    private String applicantType;

    @Transient
    private List<SurchargeInterface> surchargeInterfaces;

    @Transient
    private List<SlabInterface> slabInterfaces;

    @Transient
    private FCAInterface fcaInterface;

    @Transient
    //commented out older single fca reference as fca needs proration, so getting fca list for the same.
    //private FCAInterface fcaInterface;
    private List<FCAInterface> fcaInterfaces;

    @Transient
    private PowerFactorInterface powerFactorInterface;

    @Transient
    private List<PowerFactorInterface> powerFactorInterfaces;

    @Transient
    private WeldingSurchargeConfigurationInterface weldingSurchargeConfigurationInterface;

    @Transient
    private List<LoadFactorIncentiveConfigurationInterface> loadFactorIncentiveConfigurationInterfaces;

    @Transient
    private IrrigationSchemeInterface irrigationSchemeInterface;

    @Transient
    private AgricultureUnitInterface agricultureUnitInterface;

    @Transient
    private List<SlabInterface> alternateSlabInterfaces;

    //for lv1.1 101
    @Transient
    private MeterRentInterface meterRentInterface;

    @Transient
    private List<ElectricityDutyInterface>  electricityDutyInterfaces;

    @Transient
    private MinimumChargeInterface minimumChargeInterface;

    @Transient
    private GMCInterface gmcInterface;

    @Transient
    private AgricultureGMCInterface agricultureGMCInterface;

    @Transient
    private List<ExcessDemandSubcategoryMappingInterface> excessDemandSubcategoryMappingInterfaces;

    @Transient
    private List<XrayConnectionFixedChargeRateInterface> xrayConnectionFixedChargeRateInterfaces;

    @Transient
    private XrayConnectionInformationInterface xrayConnectionInformationInterface;

    @Transient
    private UnmeteredUnitInterface unmeteredUnitInterface;

    @Transient
    private List<SubsidyInterface> subsidyInterfaces;

    @Transient
    private List<PurposeSubsidyMappingInterface> purposeSubsidyMappingInterfaces;

    @Transient
    private EmployeeRebateInterface employeeRebateInterface;

    public String getApplicantType() {
        return applicantType;
    }

    public void setApplicantType(String applicantType) {
        this.applicantType = applicantType;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTariffId() {
        return tariffId;
    }

    public void setTariffId(long tariffId) {
        this.tariffId = tariffId;
    }

    public long getCode() {
        return code;
    }

    public void setCode(long code) {
        this.code = code;
    }

    public String getSlabId() {
        return slabId;
    }

    public void setSlabId(String slabId) {
        this.slabId = slabId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getStartContractDemandKW() {
        return startContractDemandKW;
    }

    public void setStartContractDemandKW(BigDecimal startContractDemandKW) {
        this.startContractDemandKW = startContractDemandKW;
    }

    public BigDecimal getEndContractDemandKW() {
        return endContractDemandKW;
    }

    public void setEndContractDemandKW(BigDecimal endContractDemandKW) {
        this.endContractDemandKW = endContractDemandKW;
    }

    public BigDecimal getStartConnectedLoadKW() {
        return startConnectedLoadKW;
    }

    public void setStartConnectedLoadKW(BigDecimal startConnectedLoadKW) {
        this.startConnectedLoadKW = startConnectedLoadKW;
    }

    public BigDecimal getEndConnectedLoadKW() {
        return endConnectedLoadKW;
    }

    public void setEndConnectedLoadKW(BigDecimal endConnectedLoadKW) {
        this.endConnectedLoadKW = endConnectedLoadKW;
    }

    public String getPremiseType() {
        return premiseType;
    }

    public void setPremiseType(String premiseType) {
        this.premiseType = premiseType;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Override
    public List<SurchargeInterface> getSurchargeInterfaces() {
        return this.surchargeInterfaces;
    }

    @Override
    public void setSurchargeInterfaces(List<SurchargeInterface> surchargeInterfaces) {
        this.surchargeInterfaces = surchargeInterfaces;
    }

    @Override
    public List<SlabInterface> getSlabInterfaces() {
        return this.slabInterfaces;
    }

    @Override
    public void setSlabInterfaces(List<SlabInterface> slabInterfaces) {
        this.slabInterfaces = slabInterfaces;
    }

    @Override
    public List<SlabInterface> getAlternateSlabInterfaces() {
        return this.alternateSlabInterfaces;
    }

    @Override
    public void setAlternateSlabInterfaces(List<SlabInterface> alternateSlabInterfaces) {
        this.alternateSlabInterfaces = alternateSlabInterfaces;
    }


    @Override
    public PowerFactorInterface getPowerFactorInterface() {
        return this.powerFactorInterface;
    }

    @Override
    public void setPowerFactorInterface(PowerFactorInterface powerFactorInterface) {
        this.powerFactorInterface = powerFactorInterface;
    }

    public List<PowerFactorInterface> getPowerFactorInterfaces() {
        return powerFactorInterfaces;
    }

    public void setPowerFactorInterfaces(List<PowerFactorInterface> powerFactorInterfaces) {
        this.powerFactorInterfaces = powerFactorInterfaces;
    }

    public WeldingSurchargeConfigurationInterface getWeldingSurchargeConfigurationInterface() {
        return weldingSurchargeConfigurationInterface;
    }

    public void setWeldingSurchargeConfigurationInterface(WeldingSurchargeConfigurationInterface weldingSurchargeConfigurationInterface) {
        this.weldingSurchargeConfigurationInterface = weldingSurchargeConfigurationInterface;
    }

    public List<LoadFactorIncentiveConfigurationInterface> getLoadFactorIncentiveConfigurationInterfaces() {
        return loadFactorIncentiveConfigurationInterfaces;
    }

    public void setLoadFactorIncentiveConfigurationInterfaces(List<LoadFactorIncentiveConfigurationInterface> loadFactorIncentiveConfigurationInterfaces) {
        this.loadFactorIncentiveConfigurationInterfaces = loadFactorIncentiveConfigurationInterfaces;
    }

    @Override
    public IrrigationSchemeInterface getIrrigationSchemeInterface() {
        return this.irrigationSchemeInterface;
    }

    @Override
    public void setIrrigationSchemeInterface(IrrigationSchemeInterface irrigationSchemeInterface) {
        this.irrigationSchemeInterface = irrigationSchemeInterface;
    }

    @Override
    public FCAInterface getFcaInterface() {
        return this.fcaInterface;
    }

    @Override
    public void setFcaInterface(FCAInterface fcaInterface) {
        this.fcaInterface = fcaInterface;
    }


    public List<FCAInterface> getFcaInterfaces() {
        return fcaInterfaces;
    }

    public void setFcaInterfaces(List<FCAInterface> fcaInterfaces) {
        this.fcaInterfaces = fcaInterfaces;
    }

    @Override
    public AgricultureUnitInterface getAgricultureUnitInterface() {
        return this.agricultureUnitInterface;
    }

    @Override
    public void setAgricultureUnitInterface(AgricultureUnitInterface agricultureUnitInterface) {
        this.agricultureUnitInterface = agricultureUnitInterface;
    }

    @Override
    public MeterRentInterface getMeterRentInterface() {
        return this.meterRentInterface;
    }

    @Override
    public void setMeterRentInterface(MeterRentInterface meterRentInterface) {
        this.meterRentInterface = meterRentInterface;
    }

    @Override
    public List<ElectricityDutyInterface> getElectricityDutyInterfaces() {
        return this.electricityDutyInterfaces;
    }

    @Override
    public void setElectricityDutyInterfaces(List<ElectricityDutyInterface> electricityDutyInterfaces) {
        this.electricityDutyInterfaces = electricityDutyInterfaces;
    }

    @Override
    public void setMinimumChargeInterface(MinimumChargeInterface minimumChargeInterface) {
        this.minimumChargeInterface = minimumChargeInterface;
    }

    @Override
    public MinimumChargeInterface getMinimumChargeInterface() {
        return this.minimumChargeInterface;
    }

    @Override
    public GMCInterface getGmcInterface() {
        return gmcInterface;
    }

    @Override
    public void setGmcInterface(GMCInterface gmcInterface) {
        this.gmcInterface = gmcInterface;
    }

    public AgricultureGMCInterface getAgricultureGMCInterface() {
        return agricultureGMCInterface;
    }

    public void setAgricultureGMCInterface(AgricultureGMCInterface agricultureGMCInterface) {
        this.agricultureGMCInterface = agricultureGMCInterface;
    }

    @Override
    public List<ExcessDemandSubcategoryMappingInterface> getExcessDemandSubcategoryMappingInterfaces() {
        return excessDemandSubcategoryMappingInterfaces;
    }

    @Override
    public void setExcessDemandSubcategoryMappingInterfaces(List<ExcessDemandSubcategoryMappingInterface> excessDemandSubcategoryMappingInterfaces) {
        this.excessDemandSubcategoryMappingInterfaces = excessDemandSubcategoryMappingInterfaces;
    }

    @Override
    public List<XrayConnectionFixedChargeRateInterface> getXrayConnectionFixedChargeRateInterfaces() {
        return xrayConnectionFixedChargeRateInterfaces;
    }

    @Override
    public void setXrayConnectionFixedChargeRateInterfaces(List<XrayConnectionFixedChargeRateInterface> xrayConnectionFixedChargeRateInterfaces) {
        this.xrayConnectionFixedChargeRateInterfaces = xrayConnectionFixedChargeRateInterfaces;
    }

    public XrayConnectionInformationInterface getXrayConnectionInformationInterface() {
        return xrayConnectionInformationInterface;
    }

    public void setXrayConnectionInformationInterface(XrayConnectionInformationInterface xrayConnectionInformationInterface) {
        this.xrayConnectionInformationInterface = xrayConnectionInformationInterface;
    }

    @Override
    public UnmeteredUnitInterface getUnmeteredUnitInterface() {
        return unmeteredUnitInterface;
    }

    @Override
    public void setUnmeteredUnitInterface(UnmeteredUnitInterface unmeteredUnitInterface) {
        this.unmeteredUnitInterface = unmeteredUnitInterface;
    }

    @Override
    public List<SubsidyInterface> getSubsidyInterfaces() {
        return subsidyInterfaces;
    }

    @Override
    public void setSubsidyInterfaces(List<SubsidyInterface> subsidyInterfaces) {
        this.subsidyInterfaces = subsidyInterfaces;
    }

    @Override
    public List<PurposeSubsidyMappingInterface> getPurposeSubsidyMappingInterfaces() {
        return purposeSubsidyMappingInterfaces;
    }

    @Override
    public void setPurposeSubsidyMappingInterfaces(List<PurposeSubsidyMappingInterface> purposeSubsidyMappingInterfaces) {
        this.purposeSubsidyMappingInterfaces = purposeSubsidyMappingInterfaces;
    }

    @Override
    public EmployeeRebateInterface getEmployeeRebateInterface() {
        return employeeRebateInterface;
    }

    @Override
    public void setEmployeeRebateInterface(EmployeeRebateInterface employeeRebateInterface) {
        this.employeeRebateInterface = employeeRebateInterface;
    }

    @Override
    public String toString() {
        return "SubCategory{" +
                "id=" + id +
                ", tariffId=" + tariffId +
                ", code=" + code +
                ", slabId='" + slabId + '\'' +
                ", description='" + description + '\'' +
                ", startContractDemandKW=" + startContractDemandKW +
                ", endContractDemandKW=" + endContractDemandKW +
                ", startConnectedLoadKW=" + startConnectedLoadKW +
                ", endConnectedLoadKW=" + endConnectedLoadKW +
                ", premiseType='" + premiseType + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", updatedBy='" + updatedBy + '\'' +
                ", createdOn=" + createdOn +
                ", updatedOn=" + updatedOn +
                ", applicantType='" + applicantType + '\'' +
                '}';
    }
}
