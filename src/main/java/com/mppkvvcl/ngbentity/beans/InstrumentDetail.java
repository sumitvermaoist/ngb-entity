package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.InstrumentDetailInterface;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by ANSHIKA on 14-07-2017.
 */

@Entity(name = "InstrumentDetail")
@Table(name = "instrument_detail")
public class InstrumentDetail implements InstrumentDetailInterface {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private long id;

    @Column(name = "pay_mode")
    private String payMode;

    @Column(name = "bank_name")
    private String bankName;

    @Column(name = "instrument_no")
    private String instrumentNo;

    @Column(name = "instrument_date")
    @Temporal(TemporalType.DATE)
    private Date instrumentDate;

    @Column(name = "micr_code")
    private String micrCode;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "dishonoured")
    private boolean dishonoured;

    @Column(name = "deleted")
    private boolean deleted;

    @Column(name = "created_by")
    private String createdBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_on")
    private Date createdOn;

    @Column(name = "updated_by")
    private String updatedBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_on")
    private Date updatedOn;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getInstrumentNo() {
        return instrumentNo;
    }

    public void setInstrumentNo(String instrumentNo) {
        this.instrumentNo = instrumentNo;
    }

    public Date getInstrumentDate() {
        return instrumentDate;
    }

    public void setInstrumentDate(Date instrumentDate) {
        this.instrumentDate = instrumentDate;
    }

    public String getMicrCode() {
        return micrCode;
    }

    public void setMicrCode(String micrCode) {
        this.micrCode = micrCode;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public boolean isDishonoured() {
        return dishonoured;
    }

    public void setDishonoured(boolean dishonoured) {
        this.dishonoured = dishonoured;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Override
    public String toString() {
        return "InstrumentDetail{" +
                "id=" + id +
                ", payMode='" + payMode + '\'' +
                ", bankName='" + bankName + '\'' +
                ", instrumentNo='" + instrumentNo + '\'' +
                ", instrumentDate=" + instrumentDate +
                ", micrCode='" + micrCode + '\'' +
                ", amount=" + amount +
                ", dishonoured=" + dishonoured +
                ", createdBy='" + createdBy + '\'' +
                ", createdOn=" + createdOn +
                ", updatedBy='" + updatedBy + '\'' +
                ", updatedOn=" + updatedOn +
                '}';
    }
}
