package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.SubCategoryInterface;
import com.mppkvvcl.ngbinterface.interfaces.TariffInterface;
import com.mppkvvcl.ngbinterface.interfaces.UnmeteredUnitInterface;

import javax.persistence.*;

/**
 * * This is Tariff bean class which mapped TARIFF table in the database
 * Entity use to mapped bean class to given name so that databse naming convesion can follow while writting query in spring boot e.g. here Class Tariff represent bean class so
 * first letter is Capital letter but using @Entity (name=tariff) then we can refer table by tariif not by Tariff
 * Table annotation used to mapped table name form backend table name
 * Id represent column as a primary key
 * GeneratedValue represented key generation identity in backend
 * Column mapped table column name with bean member name
 * Created by Girjesh kumar Suryawanshi on 5/19/2017.
 *
 * Updated by SUMIT VERMA on 03/06/2017
 * UPDATION : Added tariff_type column mapping for considering SEASONAL consumer with bean member variable tariffType.
 * This column again can be taken as new dimension for filtering tariffCodes beside meteringStatus and connectionType
 */

@Entity(name = "tariff")
@Table(name = "tariff")
public class Tariff implements TariffInterface {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "tariff_category")
    private String tariffCategory;

    @Column(name = "tariff_code")
    private String tariffCode;

    @Column(name = "effective_start_date")
    private String effectiveStartDate;

    @Column(name = "effective_end_date")
    private String effectiveEndDate;

    @Column(name = "metering_status")
    private String meteringStatus;

    @Column(name = "connection_type")
    private String connectionType;


    @Column(name = "description")
    private String description;


    @Column(name = "created_on")
    private String createdOn;

    @Column(name = "created_by")
    private String createdBy;


    @Column(name = "updated_on")
    private String updatedOn;


    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "tariff_type")
    private String tariffType;

    @Transient
    private SubCategoryInterface subCategoryInterface;

    @Transient
    private SubCategoryInterface alternateSubCategoryInterface;

    @Transient
    private UnmeteredUnitInterface unmeteredUnitInterface;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTariffType() {
        return tariffType;
    }

    public void setTariffType(String tariffType) {
        this.tariffType = tariffType;
    }

    public String getTariffCategory() {
        return tariffCategory;
    }

    public void setTariffCategory(String tariffCategory) {
        this.tariffCategory = tariffCategory;
    }

    public String getTariffCode() {
        return tariffCode;
    }

    public void setTariffCode(String tariffCode) {
        this.tariffCode = tariffCode;
    }

    public String getEffectiveStartDate() {
        return effectiveStartDate;
    }

    public void setEffectiveStartDate(String effectiveStartDate) {
        this.effectiveStartDate = effectiveStartDate;
    }

    public String getEffectiveEndDate() {
        return effectiveEndDate;
    }

    public void setEffectiveEndDate(String effectiveEndDate) {
        this.effectiveEndDate = effectiveEndDate;
    }

    public String getMeteringStatus() {
        return meteringStatus;
    }

    public void setMeteringStatus(String meteringStatus) {
        this.meteringStatus = meteringStatus;
    }

    public String getConnectionType() {
        return connectionType;
    }

    public void setConnectionType(String connectionType) {
        this.connectionType = connectionType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public void setSubCategoryInterface(SubCategoryInterface subCategoryInterface) {
        this.subCategoryInterface = subCategoryInterface;
    }

    @Override
    public SubCategoryInterface getSubCategoryInterface() {
        return this.subCategoryInterface;
    }

    @Override
    public void setAlternateSubCategoryInterface(SubCategoryInterface subCategoryInterface) {
        this.alternateSubCategoryInterface = subCategoryInterface;
    }

    @Override
    public SubCategoryInterface getAlternateSubCategoryInterface() {
        return this.alternateSubCategoryInterface;
    }

    @Override
    public UnmeteredUnitInterface getUnmeteredUnitInterface() {
        return unmeteredUnitInterface;
    }

    @Override
    public void setUnmeteredUnitInterface(UnmeteredUnitInterface unmeteredUnitInterface) {
        this.unmeteredUnitInterface = unmeteredUnitInterface;
    }

    @Override
    public String toString() {
        return "Tariff{" +
                "id=" + id +
                ", tariffCategory='" + tariffCategory + '\'' +
                ", tariffCode='" + tariffCode + '\'' +
                ", effectiveStartDate='" + effectiveStartDate + '\'' +
                ", effectiveEndDate='" + effectiveEndDate + '\'' +
                ", meteringStatus='" + meteringStatus + '\'' +
                ", connectionType='" + connectionType + '\'' +
                ", description='" + description + '\'' +
                ", createdOn='" + createdOn + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", updatedOn='" + updatedOn + '\'' +
                ", updatedBy='" + updatedBy + '\'' +
                ", tariffType='" + tariffType + '\'' +
                '}';
    }
}
