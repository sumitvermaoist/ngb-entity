package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.RoundingModeInterface;
import com.mppkvvcl.ngbinterface.interfaces.SurchargeInterface;

import javax.persistence.*;
import java.math.BigDecimal;

@Table(name = "surcharge")
@Entity(name = "Surcharge")
public class Surcharge implements SurchargeInterface {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "tariff_id")
    private long tariffId;
    @Column(name = "subcategory_code")
    private long subcategoryCode;
    @Column(name = "outstanding_amount")
    private BigDecimal outstandingAmount;
    @Column(name = "minimum_surcharge_amount")
    private BigDecimal minimumSuchargeAmount;
    @Column(name = "rate")
    private String rate;
    @Column(name = "multiplier")
    private BigDecimal multiplier;
    @Column(name = "rounding_mode_code")
    private int roundingModeCode;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTariffId() {
        return tariffId;
    }

    public void setTariffId(long tariffId) {
        this.tariffId = tariffId;
    }

    public long getSubcategoryCode() {
        return subcategoryCode;
    }

    public void setSubcategoryCode(long subcategoryCode) {
        this.subcategoryCode = subcategoryCode;
    }

    public BigDecimal getOutstandingAmount() {
        return outstandingAmount;
    }

    public void setOutstandingAmount(BigDecimal outstandingAmount) {
        this.outstandingAmount = outstandingAmount;
    }

    public BigDecimal getMinimumSuchargeAmount() {
        return minimumSuchargeAmount;
    }

    public void setMinimumSuchargeAmount(BigDecimal minimumSuchargeAmount) {
        this.minimumSuchargeAmount = minimumSuchargeAmount;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public BigDecimal getMultiplier() {
        if(this.multiplier != null){
            double temp = this.multiplier.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return multiplier;
    }

    public void setMultiplier(BigDecimal multiplier) {
        this.multiplier = multiplier;
    }

    public int getRoundingModeCode() {
        return roundingModeCode;
    }

    public void setRoundingModeCode(int roundingModeCode) {
        this.roundingModeCode = roundingModeCode;
    }

    @Override
    public RoundingModeInterface getRoundingModeInterface() {
        return null;
    }

    @Override
    public void setRoundingModeInterface(RoundingModeInterface roundingModeInterface) {

    }

    @Override
    public String toString() {
        return "Surcharge{" +
                "id=" + id +
                ", tariffId=" + tariffId +
                ", subcategoryCode=" + subcategoryCode +
                ", outstandingAmount=" + outstandingAmount +
                ", minimumSuchargeAmount=" + minimumSuchargeAmount +
                ", rate='" + rate + '\'' +
                ", multiplier=" + multiplier +
                ", roundingModeCode=" + roundingModeCode +
                '}';
    }
}
