package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.WindowDetailInterface;

import javax.persistence.*;

/**
 * Created by PREETESH on 7/25/2017.
 */
@Entity(name = "WindowDetail")
@Table(name = "window_detail")
public class WindowDetail implements WindowDetailInterface {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private long id;

    @Column(name = "location_code")
    private String locationCode;

    @Column(name = "name")
    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "WindowDetail{" +
                "id=" + id +
                ", locationCode='" + locationCode + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
