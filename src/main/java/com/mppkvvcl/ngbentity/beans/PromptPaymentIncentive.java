package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.PromptPaymentIncentiveInterface;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity(name = "PromptPaymentIncentive")
@Table(name = "prompt_payment_incentive")
public class PromptPaymentIncentive implements PromptPaymentIncentiveInterface{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "minimum_amount")
    private BigDecimal minimumAmount;

    @Column(name = "maximum_amount")
    private BigDecimal maximumAmount;

    @Column(name = "rate")
    private String rate;

    @Column(name = "multiplier")
    private BigDecimal multiplier;

    @Column(name = "effective_start_date")
    @Temporal(TemporalType.DATE)
    private Date effectiveStartDate;

    @Column(name = "effective_end_date")
    @Temporal(TemporalType.DATE)
    private Date effectiveEndDate;

    @Column(name = "day")
    private int day;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BigDecimal getMinimumAmount() {
        if(this.minimumAmount != null){
            double temp = this.minimumAmount.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return minimumAmount;
    }

    public void setMinimumAmount(BigDecimal minimumAmount) {
        this.minimumAmount = minimumAmount;
    }

    public BigDecimal getMaximumAmount() {
        if(this.maximumAmount != null){
            double temp = this.maximumAmount.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return maximumAmount;
    }

    public void setMaximumAmount(BigDecimal maximumAmount) {
        this.maximumAmount = maximumAmount;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public BigDecimal getMultiplier() {
        if(this.multiplier != null){
            double temp = this.multiplier.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return multiplier;
    }

    public void setMultiplier(BigDecimal multiplier) {
        this.multiplier = multiplier;
    }

    public Date getEffectiveStartDate() {
        return effectiveStartDate;
    }

    public void setEffectiveStartDate(Date effectiveStartDate) {
        this.effectiveStartDate = effectiveStartDate;
    }

    public Date getEffectiveEndDate() {
        return effectiveEndDate;
    }

    public void setEffectiveEndDate(Date effectiveEndDate) {
        this.effectiveEndDate = effectiveEndDate;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    @Override
    public String toString() {
        return "PromptPaymentIncentive{" +
                "id=" + id +
                ", minimumAmount=" + minimumAmount +
                ", maximumAmount=" + maximumAmount +
                ", rate='" + rate + '\'' +
                ", multiplier=" + multiplier +
                ", effectiveStartDate=" + effectiveStartDate +
                ", effectiveEndDate=" + effectiveEndDate +
                ", day=" + day +
                '}';
    }
}
