package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.SeasonalConnectionInformationInterface;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by PREETESH on 6/8/2017.
 */
@Entity(name = "SeasonalConnectionInformation")
@Table(name = "seasonal_connection_information")
public class SeasonalConnectionInformation implements SeasonalConnectionInformationInterface {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "consumer_no")
    private String consumerNo;

    @Column(name = "season_start_date")
    private Date seasonStartDate;

    @Column(name = "season_start_bill_month")
    private String seasonStartBillMonth;

    @Column(name = "season_end_date")
    private Date seasonEndDate;

    @Column(name = "season_end_bill_month")
    private String seasonEndBillMonth;

    @Column(name = "season_sub_category")
    private long seasonSubCategory;

    @Column(name = "season_duration")
    private int seasonDuration;

    @Column(name = "violation_month")
    private String violationMonth;

    @Column(name = "season_average_consumption")
    private BigDecimal seasonAverageConsumption;

    @Column(name = "consumption_violation_parameter")
    private BigDecimal consumptionViolationParameter;

    @Column(name = "maximum_demand_violation_parameter")
    private BigDecimal maximumDemandViolationParameter;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    @Column(name = "updated_by")
    private String updatedBy;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getConsumerNo() {
        return consumerNo;
    }

    public void setConsumerNo(String consumerNo) {
        this.consumerNo = consumerNo;
    }

    public Date getSeasonStartDate() {
        return seasonStartDate;
    }

    public void setSeasonStartDate(Date seasonStartDate) {
        this.seasonStartDate = seasonStartDate;
    }

    public String getSeasonStartBillMonth() {
        return seasonStartBillMonth;
    }

    public void setSeasonStartBillMonth(String seasonStartBillMonth) {
        this.seasonStartBillMonth = seasonStartBillMonth;
    }

    public Date getSeasonEndDate() {
        return seasonEndDate;
    }

    public void setSeasonEndDate(Date seasonEndDate) {
        this.seasonEndDate = seasonEndDate;
    }

    public String getSeasonEndBillMonth() {
        return seasonEndBillMonth;
    }

    public void setSeasonEndBillMonth(String seasonEndBillMonth) {
        this.seasonEndBillMonth = seasonEndBillMonth;
    }

    public long getSeasonSubCategory() {
        return seasonSubCategory;
    }

    public void setSeasonSubCategory(long seasonSubCategory) {
        this.seasonSubCategory = seasonSubCategory;
    }

    public int getSeasonDuration() {
        return seasonDuration;
    }

    public void setSeasonDuration(int seasonDuration) {
        this.seasonDuration = seasonDuration;
    }

    public String getViolationMonth() {
        return violationMonth;
    }

    public void setViolationMonth(String violationMonth) {
        this.violationMonth = violationMonth;
    }

    public BigDecimal getSeasonAverageConsumption() {
        return seasonAverageConsumption;
    }

    public void setSeasonAverageConsumption(BigDecimal seasonAverageConsumption) {
        this.seasonAverageConsumption = seasonAverageConsumption;
    }

    public BigDecimal getConsumptionViolationParameter() {
        return consumptionViolationParameter;
    }

    public void setConsumptionViolationParameter(BigDecimal consumptionViolationParameter) {
        this.consumptionViolationParameter = consumptionViolationParameter;
    }

    public BigDecimal getMaximumDemandViolationParameter() {
        return maximumDemandViolationParameter;
    }

    public void setMaximumDemandViolationParameter(BigDecimal maximumDemandViolationParameter) {
        this.maximumDemandViolationParameter = maximumDemandViolationParameter;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public String toString() {
        return "SeasonalConnectionInformation{" +
                "id=" + id +
                ", consumerNo='" + consumerNo + '\'' +
                ", seasonStartDate=" + seasonStartDate +
                ", seasonStartBillMonth='" + seasonStartBillMonth + '\'' +
                ", seasonEndDate=" + seasonEndDate +
                ", seasonEndBillMonth='" + seasonEndBillMonth + '\'' +
                ", seasonSubCategory=" + seasonSubCategory +
                ", seasonDuration=" + seasonDuration +
                ", violationMonth='" + violationMonth + '\'' +
                ", seasonAverageConsumption=" + seasonAverageConsumption +
                ", consumptionViolationParameter=" + consumptionViolationParameter +
                ", maximumDemandViolationParameter=" + maximumDemandViolationParameter +
                ", createdOn=" + createdOn +
                ", createdBy='" + createdBy + '\'' +
                ", updatedOn=" + updatedOn +
                ", updatedBy='" + updatedBy + '\'' +
                '}';
    }
}
