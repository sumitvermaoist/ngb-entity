package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.SlabInterface;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by MITHLESH on 14-07-2017.
 */
@Entity(name = "Slab")
@Table(name = "Slab")
public class Slab implements SlabInterface {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "tariff_id")
    private long tariffId;

    @Column(name = "slab_id")
    private String slabId;

    @Column(name = "slab_start")
    private long slabStart;

    @Column(name = "slab_end")
    private long slabEnd;

    @Column(name = "rate")
    private BigDecimal rate;

    @Column(name = "fixed_charge_urban")
    private BigDecimal fixedChargeUrban;

    @Column(name = "fixed_charge_rural")
    private BigDecimal fixedChargeRural;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTariffId() {
        return tariffId;
    }

    public void setTariffId(long tariffId) {
        this.tariffId = tariffId;
    }

    public String getSlabId() {
        return slabId;
    }

    public void setSlabId(String slabId) {
        this.slabId = slabId;
    }

    public long getSlabStart() {
        return slabStart;
    }

    public void setSlabStart(long slabStart) {
        this.slabStart = slabStart;
    }

    public long getSlabEnd() {
        return slabEnd;
    }

    public void setSlabEnd(long slabEnd) {
        this.slabEnd = slabEnd;
    }

    public BigDecimal getRate() {
        if(this.rate != null){
            double temp = this.rate.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public BigDecimal getFixedChargeUrban() {
        if(this.fixedChargeUrban != null){
            double temp = this.fixedChargeUrban.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return fixedChargeUrban;
    }

    public void setFixedChargeUrban(BigDecimal fixedChargeUrban) {
        this.fixedChargeUrban = fixedChargeUrban;
    }

    public BigDecimal getFixedChargeRural() {
        if(this.fixedChargeRural != null){
            double temp = this.fixedChargeRural.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return fixedChargeRural;
    }

    public void setFixedChargeRural(BigDecimal fixedChargeRural) {
        this.fixedChargeRural = fixedChargeRural;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Override
    public String toString() {
        return "Slab{" +
                "id=" + id +
                ", tariffId=" + tariffId +
                ", slabId='" + slabId + '\'' +
                ", slabStart=" + slabStart +
                ", slabEnd=" + slabEnd +
                ", rate=" + rate +
                ", fixedChargeUrban=" + fixedChargeUrban +
                ", fixedChargeRural=" + fixedChargeRural +
                ", createdBy='" + createdBy + '\'' +
                ", createdOn=" + createdOn +
                ", updatedBy='" + updatedBy + '\'' +
                ", updatedOn=" + updatedOn +
                '}';
    }
}
