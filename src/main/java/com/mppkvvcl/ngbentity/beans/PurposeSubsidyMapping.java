package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.PurposeOfInstallationInterface;
import com.mppkvvcl.ngbinterface.interfaces.PurposeSubCategoryMappingInterface;
import com.mppkvvcl.ngbinterface.interfaces.PurposeSubsidyMappingInterface;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "PurposeSubsidyMapping")
@Table(name = "purpose_subsidy_mapping")
public class PurposeSubsidyMapping implements PurposeSubsidyMappingInterface{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "subcategory_code")
    private long subcategoryCode;

    @Column(name = "purpose_of_installation_id")
    private long purposeOfInstallationId;

    @Column(name = "effective_start_date")
    private Date effectiveStartDate;

    @Column(name = "effective_end_date")
    private Date effectiveEndDate;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getSubcategoryCode() {
        return subcategoryCode;
    }

    public void setSubcategoryCode(long subcategoryCode) {
        this.subcategoryCode = subcategoryCode;
    }

    public long getPurposeOfInstallationId() {
        return purposeOfInstallationId;
    }

    public void setPurposeOfInstallationId(long purposeOfInstallationId) {
        this.purposeOfInstallationId = purposeOfInstallationId;
    }

    public Date getEffectiveStartDate() {
        return effectiveStartDate;
    }

    public void setEffectiveStartDate(Date effectiveStartDate) {
        this.effectiveStartDate = effectiveStartDate;
    }

    public Date getEffectiveEndDate() {
        return effectiveEndDate;
    }

    public void setEffectiveEndDate(Date effectiveEndDate) {
        this.effectiveEndDate = effectiveEndDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    @Override
    public String toString() {
        return "PurposeSubsidyMapping{" +
                "id=" + id +
                ", subcategoryCode=" + subcategoryCode +
                ", purposeOfInstallationId=" + purposeOfInstallationId +
                ", effectiveStartDate=" + effectiveStartDate +
                ", effectiveEndDate=" + effectiveEndDate +
                ", createdBy='" + createdBy + '\'' +
                ", createdOn=" + createdOn +
                '}';
    }
}
