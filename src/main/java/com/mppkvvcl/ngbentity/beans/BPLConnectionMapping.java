package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.BPLConnectionMappingInterface;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by PREETESH on 6/8/2017.
 */

@Entity(name = "BPLConnectionMapping")
@Table(name = "bpl_connection_mapping")
public class BPLConnectionMapping implements BPLConnectionMappingInterface {


    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "bpl_no")
    private String bplNo;

    @Column(name = "consumer_no")
    private String consumerNo;

    @Column(name = "status")
    private String status;

    @Column(name = "start_date")
    @Temporal(TemporalType.DATE)
    private Date startDate;

    @Column(name = "start_bill_month")
    private String startBillMonth;

    @Column(name = "end_date")
    @Temporal(TemporalType.DATE)
    private Date endDate;

    @Column(name = "end_bill_month")
    private String endBillMonth;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    @Override
    public String toString() {
        return "BPLConnectionMapping{" +
                "id=" + id +
                ", bplNo='" + bplNo + '\'' +
                ", consumerNo='" + consumerNo + '\'' +
                ", status='" + status + '\'' +
                ", startDate=" + startDate +
                ", startBillMonth='" + startBillMonth + '\'' +
                ", endDate=" + endDate +
                ", endBillMonth='" + endBillMonth + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", updatedBy='" + updatedBy + '\'' +
                ", createdOn=" + createdOn +
                ", updatedOn=" + updatedOn +
                '}';
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getBplNo() {
        return bplNo;
    }

    public void setBplNo(String bplNo) {
        this.bplNo = bplNo;
    }

    public String getConsumerNo() {
        return consumerNo;
    }

    public void setConsumerNo(String consumerNo) {
        this.consumerNo = consumerNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getStartBillMonth() {
        return startBillMonth;
    }

    public void setStartBillMonth(String startBillMonth) {
        this.startBillMonth = startBillMonth;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getEndBillMonth() {
        return endBillMonth;
    }

    public void setEndBillMonth(String endBillMonth) {
        this.endBillMonth = endBillMonth;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }
}
