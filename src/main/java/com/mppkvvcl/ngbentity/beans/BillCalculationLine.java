package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.BillCalculationLineInterface;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by PREETESH on 12/12/2017.
 */
@Entity(name = "BillCalculationLine")
@Table(name = "bill_calculation_line")
public class BillCalculationLine implements BillCalculationLineInterface {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "bill_id")
    private long billId;

    @Column(name = "head")
    private String head;

    @Column(name = "unit")
    private String unit;

    @Column(name = "rate")
    private String rate;

    @Column(name = "amount")
    private String amount;

    @Column(name = "start_slab")
    private String startSlab;

    @Column(name = "end_slab")
    private String endSlab;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public long getBillId() {
        return billId;
    }

    @Override
    public void setBillId(long billId) {
        this.billId = billId;
    }

    @Override
    public String getHead() {
        return head;
    }

    @Override
    public void setHead(String head) {
        this.head = head;
    }

    @Override
    public String getUnit() {
        return unit;
    }

    @Override
    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    @Override
    public String getAmount() {
        return amount;
    }

    @Override
    public void setAmount(String amount) {
        this.amount = amount;
    }

    @Override
    public String getStartSlab() {
        return startSlab;
    }

    @Override
    public void setStartSlab(String startSlab) {
        this.startSlab = startSlab;
    }

    @Override
    public String getEndSlab() {
        return endSlab;
    }

    @Override
    public void setEndSlab(String endSlab) {
        this.endSlab = endSlab;
    }

    @Override
    public String getCreatedBy() {
        return createdBy;
    }

    @Override
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public Date getCreatedOn() {
        return createdOn;
    }

    @Override
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }
}
