package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.ConsumerNoGeneratorInterface;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by SUMIT on 20-06-2017.
 */
@Entity(name = "ConsumerNoGenerator")
@Table(name = "consumer_no_generator")
public class ConsumerNoGenerator implements ConsumerNoGeneratorInterface {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "app_prefix")
    private String appPrefix;

    @Column(name = "discom_code")
    private String discomCode;

    @Column(name = "location_code")
    private String locationCode;

    @Column(name = "trailing_code")
    private String trailingCode;

    @Column(name = "latest_consumer_no")
    private long latestConsumerNo;

    @Column(name = "total_consumer_no_generated")
    private long totalConsumerNoGenerated;

    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "updated_by")
    private String updatedBy;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAppPrefix() {
        return appPrefix;
    }

    public void setAppPrefix(String appPrefix) {
        this.appPrefix = appPrefix;
    }

    public String getDiscomCode() {
        return discomCode;
    }

    public void setDiscomCode(String discomCode) {
        this.discomCode = discomCode;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getTrailingCode() {
        return trailingCode;
    }

    public void setTrailingCode(String trailingCode) {
        this.trailingCode = trailingCode;
    }

    public long getLatestConsumerNo() {
        return latestConsumerNo;
    }

    public void setLatestConsumerNo(long latestConsumerNo) {
        this.latestConsumerNo = latestConsumerNo;
    }

    public long getTotalConsumerNoGenerated() {
        return totalConsumerNoGenerated;
    }

    public void setTotalConsumerNoGenerated(long totalConsumerNoGenerated) {
        this.totalConsumerNoGenerated = totalConsumerNoGenerated;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public String toString() {
        return "ConsumerNoGenerator{" +
                "id=" + id +
                ", discomCode='" + discomCode + '\'' +
                ", locationCode='" + locationCode + '\'' +
                ", trailingCode='" + trailingCode + '\'' +
                ", latestConsumerNo=" + latestConsumerNo +
                ", totalConsumerNoGenerated=" + totalConsumerNoGenerated +
                ", updatedOn=" + updatedOn +
                ", createdOn=" + createdOn +
                ", createdBy='" + createdBy + '\'' +
                ", updatedBy='" + updatedBy + '\'' +
                '}';
    }
}
