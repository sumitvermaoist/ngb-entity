package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.ISIEnergySavingTypeInterface;

import javax.persistence.*;

/**
 * Created by PREETESH on 6/8/2017.
 */
@Entity(name = "ISIEnergySavingType")
@Table(name = "isi_energy_saving_type")
public class ISIEnergySavingType implements ISIEnergySavingTypeInterface {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "type")
    private String type;

    @Column(name = "description")
    private String description;

    @Override
    public String toString() {
        return "ISIEnergySavingType{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
