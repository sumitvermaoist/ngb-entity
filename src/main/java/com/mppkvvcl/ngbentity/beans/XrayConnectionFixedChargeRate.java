package com.mppkvvcl.ngbentity.beans;

/**
 * Created by PREETESH on 12/18/2017.
 */

import com.mppkvvcl.ngbinterface.interfaces.XrayConnectionFixedChargeRateInterface;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity(name = "XrayConnectionFixedChargeRate")
@Table(name = "xray_connection_fixed_charge_rate")
public class XrayConnectionFixedChargeRate implements XrayConnectionFixedChargeRateInterface {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "tariff_id")
    private long tariffId;

    @Column(name = "subcategory_code")
    private long subcategoryCode;

    @Column(name = "xray_type")
    private String xrayType;

    @Column(name = "rate")
    private String rate;

    @Column(name = "multiplier")
    private BigDecimal multiplier;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "created_by")
    private String createdBy;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTariffId() {
        return tariffId;
    }

    public void setTariffId(long tariffId) {
        this.tariffId = tariffId;
    }

    public long getSubcategoryCode() {
        return subcategoryCode;
    }

    public void setSubcategoryCode(long subcategoryCode) {
        this.subcategoryCode = subcategoryCode;
    }

    public String getXrayType() {
        return xrayType;
    }

    public void setXrayType(String xrayType) {
        this.xrayType = xrayType;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public BigDecimal getMultiplier() {
        if(this.multiplier != null){
            double temp = this.multiplier.doubleValue();
            return new BigDecimal(String.valueOf(temp));
        }
        return multiplier;
    }

    public void setMultiplier(BigDecimal multiplier) {
        this.multiplier = multiplier;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public String toString() {
        return "XrayConnectionFixedChargeRate{" +
                "id=" + id +
                ", tariffId=" + tariffId +
                ", subcategoryCode=" + subcategoryCode +
                ", xrayType='" + xrayType + '\'' +
                ", rate='" + rate + '\'' +
                ", multiplier=" + multiplier +
                ", createdOn=" + createdOn +
                ", createdBy='" + createdBy + '\'' +
                '}';
    }
}
