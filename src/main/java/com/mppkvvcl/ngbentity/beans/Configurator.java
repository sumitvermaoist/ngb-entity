package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.ConfiguratorInterface;

import javax.persistence.*;

/**
 * Created by PREETESH on 8/22/2017.
 */
@Entity(name = "Configurator")
@Table(name = "configurator")
public class Configurator implements ConfiguratorInterface {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "code")
    private String code;

    @Column(name = "value")
    private long value;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Configurator{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", value=" + value +
                '}';
    }
}
