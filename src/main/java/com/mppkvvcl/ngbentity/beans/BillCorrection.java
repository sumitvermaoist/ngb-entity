package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.BillCorrectionInterface;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Vikas on 12/27/2017.
 */
@Entity(name = "BillCorrection")
@Table(name = "bill_correction")
public class BillCorrection implements BillCorrectionInterface {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "bill_id")
    private long billId;

    @Column(name = "reference_bill_id")
    private long referenceBillId;

    @Column(name = "location_code")
    private String locationCode;

    @Column(name = "group_no")
    private String groupNo;

    @Column(name = "reading_diary_no")
    private String readingDiaryNo;

    @Column(name = "consumer_no")
    private String consumerNo;

    @Column(name = "bill_month")
    private String billMonth;

    @Column(name = "bill_type_code")
    private String billTypeCode;

    @Column(name = "bill_date")
    @Temporal(TemporalType.DATE)
    private Date billDate;

    @Column(name = "due_date")
    @Temporal(TemporalType.DATE)
    private Date dueDate;

    @Column(name = "cheque_due_date")
    @Temporal(TemporalType.DATE)
    private Date chequeDueDate;

    @Column(name = "current_read_date")
    @Temporal(TemporalType.DATE)
    private Date currentReadDate;

    @Column(name = "current_read")
    private BigDecimal currentRead;

    @Column(name = "previous_read")
    private BigDecimal previousRead;

    @Column(name = "difference")
    private BigDecimal difference;

    @Column(name = "mf")
    private BigDecimal mf;

    @Column(name = "metered_unit")
    private BigDecimal meteredUnit;

    @Column(name = "assessment")
    private BigDecimal assessment;

    @Column(name = "total_unit")
    private BigDecimal totalUnit;

    @Column(name = "gmc_unit")
    private BigDecimal gmcUnit;

    @Column(name = "billed_unit")
    private BigDecimal billedUnit;

    @Column(name = "billed_md")
    private BigDecimal billedMD;

    @Column(name = "billed_pf")
    private BigDecimal billedPF;

    @Column(name = "load_factor")
    private BigDecimal loadFactor;

    @Column(name = "fixed_charge")
    private BigDecimal fixedCharge;

    @Column(name = "additional_fixed_charges1")
    private BigDecimal additionalFixedCharges1;

    @Column(name = "additional_fixed_charges2")
    private BigDecimal additionalFixedCharges2;

    @Column(name = "energy_charge")
    private BigDecimal energyCharge;

    @Column(name = "fca_charge")
    private BigDecimal fcaCharge;

    @Column(name = "electricity_duty")
    private BigDecimal electricityDuty;

    @Column(name = "meter_rent")
    private BigDecimal meterRent;

    @Column(name = "pf_charge")
    private BigDecimal pfCharge;

    @Column(name = "welding_transformer_surcharge")
    private BigDecimal weldingTransformerSurcharge;

    @Column(name = "load_factor_incentive")
    private BigDecimal loadFactorIncentive;

    @Column(name = "sd_interest")
    private BigDecimal sdInterest;

    @Column(name = "ccb_adjustment")
    private BigDecimal ccbAdjustment;

    @Column(name = "lock_credit")
    private BigDecimal lockCredit;

    @Column(name = "other_adjustment")
    private BigDecimal otherAdjustment;

    @Column(name = "employee_rebate")
    private BigDecimal employeeRebate;

    @Column(name = "online_payment_rebate")
    private BigDecimal onlinePaymentRebate;

    @Column(name = "prepaid_meter_rebate")
    private BigDecimal prepaidMeterRebate;

    @Column(name = "prompt_payment_incentive")
    private BigDecimal promptPaymentIncentive;

    @Column(name = "advance_payment_incentive")
    private BigDecimal advancePaymentIncentive;

    @Column(name = "demand_side_incentive")
    private BigDecimal demandSideIncentive;

    @Column(name = "subsidy")
    private BigDecimal subsidy;

    @Column(name = "current_bill")
    private BigDecimal currentBill;

    @Column(name = "arrear")
    private BigDecimal arrear;

    @Column(name = "cumulative_surcharge")
    private BigDecimal cumulativeSurcharge;

    @Column(name = "surcharge_demanded")
    private BigDecimal surchargeDemanded;

    @Column(name = "net_bill")
    private BigDecimal netBill;

    @Column(name = "asd_billed")
    private BigDecimal asdBilled;

    @Column(name = "asd_arrear")
    private BigDecimal asdArrear;

    @Column(name = "asd_installment")
    private BigDecimal asdInstallment;

    @Column(name = "deleted")
    private boolean deleted;

    @Column(name = "pristine_electricity_duty")
    private BigDecimal pristineElectricityDuty;

    @Column(name = "pristine_net_bill")
    private BigDecimal pristineNetBill;

    @Column(name = "current_bill_surcharge")
    private BigDecimal currentBillSurcharge;

    @Column(name = "xray_fixed_charge")
    private BigDecimal xrayFixedCharge;

    @Column(name = "used_on_mis")
    private boolean usedOnMis;


    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getBillId() {
        return billId;
    }

    public void setBillId(long billId) {
        this.billId = billId;
    }

    public long getReferenceBillId() {
        return referenceBillId;
    }

    public void setReferenceBillId(long referenceBillId) {
        this.referenceBillId = referenceBillId;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getGroupNo() {
        return groupNo;
    }

    public void setGroupNo(String groupNo) {
        this.groupNo = groupNo;
    }

    public String getReadingDiaryNo() {
        return readingDiaryNo;
    }

    public void setReadingDiaryNo(String readingDiaryNo) {
        this.readingDiaryNo = readingDiaryNo;
    }

    public String getConsumerNo() {
        return consumerNo;
    }

    public void setConsumerNo(String consumerNo) {
        this.consumerNo = consumerNo;
    }

    public String getBillMonth() {
        return billMonth;
    }

    public void setBillMonth(String billMonth) {
        this.billMonth = billMonth;
    }

    public String getBillTypeCode() {
        return billTypeCode;
    }

    public void setBillTypeCode(String billTypeCode) {
        this.billTypeCode = billTypeCode;
    }

    public Date getBillDate() {
        return billDate;
    }

    public void setBillDate(Date billDate) {
        this.billDate = billDate;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Date getChequeDueDate() {
        return chequeDueDate;
    }

    public void setChequeDueDate(Date chequeDueDate) {
        this.chequeDueDate = chequeDueDate;
    }

    public Date getCurrentReadDate() {
        return currentReadDate;
    }

    public void setCurrentReadDate(Date currentReadDate) {
        this.currentReadDate = currentReadDate;
    }

    public BigDecimal getCurrentRead() {
        return currentRead;
    }

    public void setCurrentRead(BigDecimal currentRead) {
        this.currentRead = currentRead;
    }

    public BigDecimal getPreviousRead() {
        return previousRead;
    }

    public void setPreviousRead(BigDecimal previousRead) {
        this.previousRead = previousRead;
    }

    public BigDecimal getDifference() {
        return difference;
    }

    public void setDifference(BigDecimal difference) {
        this.difference = difference;
    }

    public BigDecimal getMf() {
        return mf;
    }

    public void setMf(BigDecimal mf) {
        this.mf = mf;
    }

    public BigDecimal getMeteredUnit() {
        return meteredUnit;
    }

    public void setMeteredUnit(BigDecimal meteredUnit) {
        this.meteredUnit = meteredUnit;
    }

    public BigDecimal getAssessment() {
        return assessment;
    }

    public void setAssessment(BigDecimal assessment) {
        this.assessment = assessment;
    }

    public BigDecimal getTotalUnit() {
        return totalUnit;
    }

    public void setTotalUnit(BigDecimal totalUnit) {
        this.totalUnit = totalUnit;
    }

    public BigDecimal getGmcUnit() {
        return gmcUnit;
    }

    public void setGmcUnit(BigDecimal gmcUnit) {
        this.gmcUnit = gmcUnit;
    }

    public BigDecimal getBilledUnit() {
        return billedUnit;
    }

    public void setBilledUnit(BigDecimal billedUnit) {
        this.billedUnit = billedUnit;
    }

    public BigDecimal getBilledMD() {
        return billedMD;
    }

    public void setBilledMD(BigDecimal billedMD) {
        this.billedMD = billedMD;
    }

    public BigDecimal getBilledPF() {
        return billedPF;
    }

    public void setBilledPF(BigDecimal billedPF) {
        this.billedPF = billedPF;
    }

    public BigDecimal getLoadFactor() {
        return loadFactor;
    }

    public void setLoadFactor(BigDecimal loadFactor) {
        this.loadFactor = loadFactor;
    }

    public BigDecimal getFixedCharge() {
        return fixedCharge;
    }

    public void setFixedCharge(BigDecimal fixedCharge) {
        this.fixedCharge = fixedCharge;
    }

    public BigDecimal getAdditionalFixedCharges1() {
        return additionalFixedCharges1;
    }

    public void setAdditionalFixedCharges1(BigDecimal additionalFixedCharges1) {
        this.additionalFixedCharges1 = additionalFixedCharges1;
    }

    public BigDecimal getAdditionalFixedCharges2() {
        return additionalFixedCharges2;
    }

    public void setAdditionalFixedCharges2(BigDecimal additionalFixedCharges2) {
        this.additionalFixedCharges2 = additionalFixedCharges2;
    }

    public BigDecimal getEnergyCharge() {
        return energyCharge;
    }

    public void setEnergyCharge(BigDecimal energyCharge) {
        this.energyCharge = energyCharge;
    }

    public BigDecimal getFcaCharge() {
        return fcaCharge;
    }

    public void setFcaCharge(BigDecimal fcaCharge) {
        this.fcaCharge = fcaCharge;
    }

    public BigDecimal getElectricityDuty() {
        return electricityDuty;
    }

    public void setElectricityDuty(BigDecimal electricityDuty) {
        this.electricityDuty = electricityDuty;
    }

    public BigDecimal getMeterRent() {
        return meterRent;
    }

    public void setMeterRent(BigDecimal meterRent) {
        this.meterRent = meterRent;
    }

    public BigDecimal getPfCharge() {
        return pfCharge;
    }

    public void setPfCharge(BigDecimal pfCharge) {
        this.pfCharge = pfCharge;
    }

    public BigDecimal getWeldingTransformerSurcharge() {
        return weldingTransformerSurcharge;
    }

    public void setWeldingTransformerSurcharge(BigDecimal weldingTransformerSurcharge) {
        this.weldingTransformerSurcharge = weldingTransformerSurcharge;
    }

    public BigDecimal getLoadFactorIncentive() {
        return loadFactorIncentive;
    }

    public void setLoadFactorIncentive(BigDecimal loadFactorIncentive) {
        this.loadFactorIncentive = loadFactorIncentive;
    }

    public BigDecimal getSdInterest() {
        return sdInterest;
    }

    public void setSdInterest(BigDecimal sdInterest) {
        this.sdInterest = sdInterest;
    }

    public BigDecimal getCcbAdjustment() {
        return ccbAdjustment;
    }

    public void setCcbAdjustment(BigDecimal ccbAdjustment) {
        this.ccbAdjustment = ccbAdjustment;
    }

    public BigDecimal getLockCredit() {
        return lockCredit;
    }

    public void setLockCredit(BigDecimal lockCredit) {
        this.lockCredit = lockCredit;
    }

    public BigDecimal getOtherAdjustment() {
        return otherAdjustment;
    }

    public void setOtherAdjustment(BigDecimal otherAdjustment) {
        this.otherAdjustment = otherAdjustment;
    }

    public BigDecimal getEmployeeRebate() {
        return employeeRebate;
    }

    public void setEmployeeRebate(BigDecimal employeeRebate) {
        this.employeeRebate = employeeRebate;
    }

    public BigDecimal getOnlinePaymentRebate() {
        return onlinePaymentRebate;
    }

    public void setOnlinePaymentRebate(BigDecimal onlinePaymentRebate) {
        this.onlinePaymentRebate = onlinePaymentRebate;
    }

    public BigDecimal getPrepaidMeterRebate() {
        return prepaidMeterRebate;
    }

    public void setPrepaidMeterRebate(BigDecimal prepaidMeterRebate) {
        this.prepaidMeterRebate = prepaidMeterRebate;
    }

    public BigDecimal getPromptPaymentIncentive() {
        return promptPaymentIncentive;
    }

    public void setPromptPaymentIncentive(BigDecimal promptPaymentIncentive) {
        this.promptPaymentIncentive = promptPaymentIncentive;
    }

    public BigDecimal getAdvancePaymentIncentive() {
        return advancePaymentIncentive;
    }

    public void setAdvancePaymentIncentive(BigDecimal advancePaymentIncentive) {
        this.advancePaymentIncentive = advancePaymentIncentive;
    }

    public BigDecimal getDemandSideIncentive() {
        return demandSideIncentive;
    }

    public void setDemandSideIncentive(BigDecimal demandSideIncentive) {
        this.demandSideIncentive = demandSideIncentive;
    }

    public BigDecimal getSubsidy() {
        return subsidy;
    }

    public void setSubsidy(BigDecimal subsidy) {
        this.subsidy = subsidy;
    }

    public BigDecimal getCurrentBill() {
        return currentBill;
    }

    public void setCurrentBill(BigDecimal currentBill) {
        this.currentBill = currentBill;
    }

    public BigDecimal getArrear() {
        return arrear;
    }

    public void setArrear(BigDecimal arrear) {
        this.arrear = arrear;
    }

    public BigDecimal getCumulativeSurcharge() {
        return cumulativeSurcharge;
    }

    public void setCumulativeSurcharge(BigDecimal cumulativeSurcharge) {
        this.cumulativeSurcharge = cumulativeSurcharge;
    }

    public BigDecimal getSurchargeDemanded() {
        return surchargeDemanded;
    }

    public void setSurchargeDemanded(BigDecimal surchargeDemanded) {
        this.surchargeDemanded = surchargeDemanded;
    }

    public BigDecimal getNetBill() {
        return netBill;
    }

    public void setNetBill(BigDecimal netBill) {
        this.netBill = netBill;
    }

    public BigDecimal getAsdBilled() {
        return asdBilled;
    }

    public void setAsdBilled(BigDecimal asdBilled) {
        this.asdBilled = asdBilled;
    }

    public BigDecimal getAsdArrear() {
        return asdArrear;
    }

    public void setAsdArrear(BigDecimal asdArrear) {
        this.asdArrear = asdArrear;
    }

    public BigDecimal getAsdInstallment() {
        return asdInstallment;
    }

    public void setAsdInstallment(BigDecimal asdInstallment) {
        this.asdInstallment = asdInstallment;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public BigDecimal getPristineElectricityDuty() {
        return pristineElectricityDuty;
    }

    public void setPristineElectricityDuty(BigDecimal pristineElectricityDuty) {
        this.pristineElectricityDuty = pristineElectricityDuty;
    }

    public BigDecimal getPristineNetBill() {
        return pristineNetBill;
    }

    public void setPristineNetBill(BigDecimal pristineNetBill) {
        this.pristineNetBill = pristineNetBill;
    }

    public BigDecimal getCurrentBillSurcharge() {
        return currentBillSurcharge;
    }

    public void setCurrentBillSurcharge(BigDecimal currentBillSurcharge) {
        this.currentBillSurcharge = currentBillSurcharge;
    }

    public BigDecimal getXrayFixedCharge() {
        return xrayFixedCharge;
    }

    public void setXrayFixedCharge(BigDecimal xrayFixedCharge) {
        this.xrayFixedCharge = xrayFixedCharge;
    }

    public boolean isUsedOnMis() {
        return usedOnMis;
    }

    public void setUsedOnMis(boolean usedOnMis) {
        this.usedOnMis = usedOnMis;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Override
    public String toString() {
        return "BillCorrection{" +
                "id=" + id +
                ", billId=" + billId +
                ", referenceBillId=" + referenceBillId +
                ", locationCode='" + locationCode + '\'' +
                ", groupNo='" + groupNo + '\'' +
                ", readingDiaryNo='" + readingDiaryNo + '\'' +
                ", consumerNo='" + consumerNo + '\'' +
                ", billMonth='" + billMonth + '\'' +
                ", billTypeCode='" + billTypeCode + '\'' +
                ", billDate=" + billDate +
                ", dueDate=" + dueDate +
                ", chequeDueDate=" + chequeDueDate +
                ", currentReadDate=" + currentReadDate +
                ", currentRead=" + currentRead +
                ", previousRead=" + previousRead +
                ", difference=" + difference +
                ", mf=" + mf +
                ", meteredUnit=" + meteredUnit +
                ", assessment=" + assessment +
                ", totalUnit=" + totalUnit +
                ", gmcUnit=" + gmcUnit +
                ", billedUnit=" + billedUnit +
                ", billedMD=" + billedMD +
                ", billedPF=" + billedPF +
                ", loadFactor=" + loadFactor +
                ", fixedCharge=" + fixedCharge +
                ", additionalFixedCharges1=" + additionalFixedCharges1 +
                ", additionalFixedCharges2=" + additionalFixedCharges2 +
                ", energyCharge=" + energyCharge +
                ", fcaCharge=" + fcaCharge +
                ", electricityDuty=" + electricityDuty +
                ", meterRent=" + meterRent +
                ", pfCharge=" + pfCharge +
                ", weldingTransformerSurcharge=" + weldingTransformerSurcharge +
                ", loadFactorIncentive=" + loadFactorIncentive +
                ", sdInterest=" + sdInterest +
                ", ccbAdjustment=" + ccbAdjustment +
                ", lockCredit=" + lockCredit +
                ", otherAdjustment=" + otherAdjustment +
                ", employeeRebate=" + employeeRebate +
                ", onlinePaymentRebate=" + onlinePaymentRebate +
                ", prepaidMeterRebate=" + prepaidMeterRebate +
                ", promptPaymentIncentive=" + promptPaymentIncentive +
                ", advancePaymentIncentive=" + advancePaymentIncentive +
                ", demandSideIncentive=" + demandSideIncentive +
                ", subsidy=" + subsidy +
                ", currentBill=" + currentBill +
                ", arrear=" + arrear +
                ", cumulativeSurcharge=" + cumulativeSurcharge +
                ", surchargeDemanded=" + surchargeDemanded +
                ", netBill=" + netBill +
                ", asdBilled=" + asdBilled +
                ", asdArrear=" + asdArrear +
                ", asdInstallment=" + asdInstallment +
                ", deleted=" + deleted +
                ", pristineElectricityDuty=" + pristineElectricityDuty +
                ", pristineNetBill=" + pristineNetBill +
                ", currentBillSurcharge=" + currentBillSurcharge +
                ", xrayFixedCharge=" + xrayFixedCharge +
                ", usedOnMis=" + usedOnMis +
                ", createdBy='" + createdBy + '\'' +
                ", createdOn=" + createdOn +
                ", updatedBy='" + updatedBy + '\'' +
                ", updatedOn=" + updatedOn +
                '}';
    }
}