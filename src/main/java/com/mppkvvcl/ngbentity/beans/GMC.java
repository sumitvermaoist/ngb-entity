package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.GMCInterface;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by ANSHIKA on 27-07-2017.
 */
@Entity(name = "GMC")
@Table(name = "gmc")
public class GMC implements GMCInterface {
   @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private long id;

    @Column(name = "subcategory_code")
    private long subcategoryCode;

    @Column(name = "load_unit")
    private String loadUnit;

    @Column(name = "minimum")
    private BigDecimal minimum;

    @Column(name = "tariff_id")
    private long tariffId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getSubCategoryCode() {
        return subcategoryCode;
    }

    public void setSubCategoryCode(long subCategoryCode) {
        this.subcategoryCode = subcategoryCode;
    }

    public String getLoadUnit() {
        return loadUnit;
    }

    public void setLoadUnit(String loadUnit) {
        this.loadUnit = loadUnit;
    }

    public BigDecimal getMinimum() {
        return minimum;
    }

    public void setMinimum(BigDecimal minimum) {
        this.minimum = minimum;
    }

    public long getTariffId() {
        return tariffId;
    }

    public void setTariffId(long tariffId) {
        this.tariffId = tariffId;
    }

    @Override
    public String toString() {
        return "GMC{" +
                "id=" + id +
                ", subcategoryCode=" + subcategoryCode +
                ", loadUnit='" + loadUnit + '\'' +
                ", minimum=" + minimum +
                ", tariffId=" + tariffId +
                '}';
    }
}
