package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.InstrumentDetailInterface;
import com.mppkvvcl.ngbinterface.interfaces.InstrumentPaymentMappingInterface;
import com.mppkvvcl.ngbinterface.interfaces.PaymentInterface;

import javax.persistence.*;

/**
 * Created by RUPALI on 14-07-2017.
 */
@Entity(name = "InstrumentPaymentMapping")
@Table(name = "instrument_payment_mapping")
public class InstrumentPaymentMapping implements InstrumentPaymentMappingInterface {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private long id;

    @Column(name = "payment_id")
    private long paymentId;

    @Column(name = "instrument_detail_id")
    private long instrumentDetailId;

    @OneToOne
    @JoinColumn(name = "instrument_detail_id",referencedColumnName = "id",insertable = false,updatable = false)
    private InstrumentDetail instrumentDetail;

    @OneToOne
    @JoinColumn(name = "payment_id",referencedColumnName = "id",insertable = false,updatable = false)
    private Payment payment;

    public InstrumentDetail getInstrumentDetail() {
        return instrumentDetail;
    }

    public void setInstrumentDetail(InstrumentDetailInterface instrumentDetail) {
        if(instrumentDetail != null && instrumentDetail instanceof InstrumentDetail){
            this.instrumentDetail = (InstrumentDetail) instrumentDetail;
        }
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(PaymentInterface payment) {
        if(payment != null && payment instanceof Payment){
            this.payment = (Payment) payment;
        }

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(long paymentId) {
        this.paymentId = paymentId;
    }

    public long getInstrumentDetailId() {
        return instrumentDetailId;
    }

    public void setInstrumentDetailId(long instrumentDetailId) {
        this.instrumentDetailId = instrumentDetailId;
    }

    @Override
    public String toString() {
        return "InstrumentPaymentMapping{" +
                "id=" + id +
                ", paymentId=" + paymentId +
                ", instrumentDetailId=" + instrumentDetailId +
                ", instrumentDetail=" + instrumentDetail +
                ", payment=" + payment +
                '}';
    }
}
