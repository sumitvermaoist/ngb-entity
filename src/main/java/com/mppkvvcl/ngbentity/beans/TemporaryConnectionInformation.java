package com.mppkvvcl.ngbentity.beans;

import com.mppkvvcl.ngbinterface.interfaces.TemporaryConnectionInformationInterface;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by PREETESH on 6/8/2017.
 */
@Entity(name = "TemporaryConnectionInformation")
@Table(name = "temporary_connection_information")
public class TemporaryConnectionInformation implements TemporaryConnectionInformationInterface {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "consumer_no")
    private String consumerNo;

    @Temporal(TemporalType.DATE)
    @Column(name = "tc_start_date")
    private Date tcStartDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "tc_end_date")
    private Date tcEndDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getConsumerNo() {
        return consumerNo;
    }

    public void setConsumerNo(String consumerNo) {
        this.consumerNo = consumerNo;
    }

    public Date getTcStartDate() {
        return tcStartDate;
    }

    public void setTcStartDate(Date tcStartDate) {
        this.tcStartDate = tcStartDate;
    }

    public Date getTcEndDate() {
        return tcEndDate;
    }

    public void setTcEndDate(Date tcEndDate) {
        this.tcEndDate = tcEndDate;
    }

    @Override
    public String toString() {
        return "TemporaryConnectionInformation{" +
                "id=" + id +
                ", consumerNo='" + consumerNo + '\'' +
                ", tcStartDate=" + tcStartDate +
                ", tcEndDate=" + tcEndDate +
                '}';
    }
}
